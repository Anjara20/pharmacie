<!DOCTYPE html>
<%@page import="model.Produit"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Products - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/modernizer.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
    integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
    crossorigin=""></script>


    <!-- Load Esri Leaflet from CDN -->
    <script src="https://unpkg.com/esri-leaflet@2.3.0/dist/esri-leaflet.js"
    integrity="sha512-1tScwpjXwwnm6tTva0l0/ZgM3rYNbdyMj5q6RSQMbNX6EUMhYDE3pMRGZaT41zHEvLoWEK7qFEJmZDOoDMU7/Q=="
    crossorigin=""></script>


  <!-- Load Esri Leaflet Geocoder from CDN -->
  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.14/dist/esri-leaflet-geocoder.css"
    integrity="sha512-v5YmWLm8KqAAmg5808pETiccEohtt8rPVMGQ1jA6jqkWVydV5Cuz3nJ9fQ7ittSxvuqsvI9RSGfVoKPaAJZ/AQ=="
    crossorigin="">
  <script src="https://unpkg.com/esri-leaflet-geocoder@2.2.14/dist/esri-leaflet-geocoder.js"
    integrity="sha512-uK5jVwR81KVTGe8KpJa1QIN4n60TsSV8+DPbL5wWlYQvb0/nYNgSOg9dZG6ViQhwx/gaMszuWllTemL+K+IXjg=="
    crossorigin=""></script>

  <style>
    body { margin:0; padding:0; }
    #map {
    width: 50%;
    height: 70vh; 
    margin-left: 25%;
    }
  </style>
</head>
<body>    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>Our Products</h2>
						<ul class="page-title-link">
							<li><a href="ModeleClient.jsp">Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div id="portfolio" class="section wb" style="margin-top:-100px">
        <div class="container">
            <div class="section-title text-center">
                <h3>Commander</h3>
                <p class="lead"> Medicament is any substance or composition presented as possessing curative <br> or preventive properties with regard to human or animal diseases </p>
            </div>
        </div>
    <form role="form" name="form" action="commande" method="get" style="float:right">
            <h4>Votre nom</h4><input name="nomclient" type="text" />
            <h4>Votre num�ro de telephone</h4><input name="contact" type="text" />
            <h4>Adresse</h4><input name="adresse" type="text" />                    
            <input type="hidden" id="name1" name="quartier" value="">
            <button type="submit" class="btn btn-success">Envoyer</button>
    </form>
    <div id="map"></div>
    <div id="produit" style="margin-top:-30%">
        <form role="form" name="form" action="rechercherproduit" method="get">
            <h4>Produit</h4><input id="produit-selector" name="produit" type="text" /><button type="submit" class="btn btn-success"> Rechercher </button>
            <ul>
                <%
                    Object motcle = request.getAttribute("produits");
                    if(motcle != null)
                    {
                        List<Produit> liste = (ArrayList<Produit>) request.getAttribute("produits");
                        int taille = liste.size();
                        for(int i = 0; i < taille; i++)
                        {
                %>
                    <li> <% out.println(liste.get(i).getNomproduit()); %> </li>
        </form>
        <form role="form" name="form" action="commande" method="post">
                    <input name="pdt" type="hidden" value="<% out.println(liste.get(i).getNomproduit()); %>"/>
                    <input name="idpdt" type="hidden" value="<% out.println(liste.get(i).getIdproduit()); %>"/>
                    <h4>Quantite</h4><input name="quantite" type="number" /><button type="submit" class="btn btn-success"> Ajouter </button>
        </form>
                <%
                        }
                    }
                %>
              </ul>          
        
    </div>
        </div>

         
       
            <script src="./js/app.js"></script>
	
	

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <img src="images/logos/logo-2.png" alt="" />
                        </div>
                        <p> Thanks to the system obtained by this project, the exchanges between the pharmaceutical shops and the customers will be considerably facilitated. The ease of access will allow the community to free itself from a burden, some worry and a little time, to devote itself to other things, without neglecting in a field as important and vital as health. The company benefiting from this system can therefore lighten up compared to their daily lives.</p>
                    </div>
                </div>

				<div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Pages</h3>
                        </div>

                        <ul class="footer-links hov">
                            <li><a href="index.jsp">Home <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="about-us.jsp">About <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="portfolio.jsp">Products <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="login.jsp">Login <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="">Panier <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="contact.jsp">Contact <span class="icon icon-arrow-right2"></span></a></li>
                        </ul>
                    </div>
                </div>
				
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-distributed widget clearfix">
                        <div class="widget-title">
                            <h3>Subscribe</h3>
							<p>Thank you for visiting our website, if you have any questions, do not hesitate to click on contact if you are satisfied, share it</p>
                        </div>
						
						<div class="footer-right">
							<form method="get" action="">
								<input placeholder="Subscribe our newsletter.." name="search">
								<i class="fa fa-envelope-o"></i>
							</form>
						</div>                        
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-left">                   
                    <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="">e-Fanafody</a>
                </div>

                
            </div>
        </div>
    </div>

    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="js/all.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/portfolio.js"></script>
    <script src="js/hoverdir.js"></script>    

</body>
</html>