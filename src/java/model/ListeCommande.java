package model;

public class ListeCommande
{
    String idCommande;
    String ordonnance;
    String adresse;
    int contact;
    String quartier;
    String nomClient;
    int etat;
    String idLivreur;
    String idDetailCommande;
    int quantiteProduit;
    String nomProduit;

    public ListeCommande() 
    {

    }

    public ListeCommande(String idCommande, String ordonnance, String adresse, int contact, String quartier, String nomClient, int etat, String idLivreur, String idDetailCommande, int quantiteProduit, String nomProduit) 
    {
        this.setIdCommande(idCommande);
        this.setOrdonnance(ordonnance);;
        this.setAdresse(adresse);
        this.setContact(contact);
        this.setQuartier(quartier);
        this.setNomClient(nomClient);
        this.setEtat(etat);
        this.setIdLivreur(idLivreur);
        this.setIdDetailCommande(idDetailCommande);
        this.setQuantiteProduit(quantiteProduit);
        this.setNomProduit(nomProduit);
    }

    public String getIdCommande() 
    {
        return this.idCommande;
    }

    public void setIdCommande(String idCommande) 
    {
        this.idCommande = idCommande;
    }

    public String getOrdonnance() 
    {
        return this.ordonnance;
    }

    public void setOrdonnance(String ordonnance) 
    {
        this.ordonnance = ordonnance;
    }

    public String getAdresse() 
    {
        return this.adresse;
    }

    public void setAdresse(String adresse) 
    {
        this.adresse = adresse;
    }

    public int getContact() 
    {
        return this.contact;
    }

    public void setContact(int contact) 
    {
        this.contact = contact;
    }

    public String getQuartier() 
    {
        return this.quartier;
    }

    public void setQuartier(String quartier) 
    {
        this.quartier = quartier;
    }

    public String getNomClient() 
    {
        return this.nomClient;
    }

    public void setNomClient(String nomClient) 
    {
        this.nomClient = nomClient;
    }

    public int getEtat() 
    {
        return this.etat;
    }

    public void setEtat(int etat) 
    {
        this.etat = etat;
    }

    public String getIdLivreur() 
    {
        return this.idLivreur;
    }

    public void setIdLivreur(String idLivreur) 
    {
        this.idLivreur = idLivreur;
    }

    public String getIdDetailCommande() 
    {
        return this.idDetailCommande;
    }

    public void setIdDetailCommande(String idDetailCommande) 
    {
        this.idDetailCommande = idDetailCommande;
    }

    public int getQuantiteProduit() 
    {
        return this.quantiteProduit;
    }

    public void setQuantiteProduit(int quantiteProduit) 
    {
        this.quantiteProduit = quantiteProduit;
    }

    public String getNomProduit() 
    {
        return this.nomProduit;
    }

    public void setNomProduit(String nomProduit) 
    {
        this.nomProduit = nomProduit;
    }
}