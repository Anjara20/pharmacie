package model;
import java.io.*;
import com.itextpdf.text.*;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import java.lang.reflect.*;
import java.sql.Date;
import dao.DbConnect;
import dao.General;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import model.DetailCommande;
import service.CommandeService;

public class PDF1 {
    Commande commande;
    String nomFile;
    public PDF1(String idDemande,String nomFile) throws Exception {
        String[] colone = {"idCommande"};
        String[] values = {idDemande};
        DbConnect co = new DbConnect();
        Commande[] call = CommandeService.selectCommande("idCommande like '"+idDemande+"'",co.connect());
        this.commande = call[0];        
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(nomFile));
        document.open();
        document.add(new Paragraph("Facture e-pharmacie"));
        document.add(new Paragraph("Détails"));
        List list = new List();  
        java.util.Date javaDate = new java.util.Date();
        java.sql.Date date = new Date(javaDate.getTime());    
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = format1.format(date); 
        list.add("Date:"+date);
        list.add("Nom:"+this.commande.getNomclient());
        list.add("Numero:"+this.commande.getIdCommande());
        list.add("A livrer à:"+this.commande.getAdresse());
        document.add(list);
        createFacture(document);
        document.close();
    }
    public void createFacture(Document document) throws Exception {
        String[] colone = {"idCommande"};
        String[] values = {this.commande.getIdCommande()};
        Object[] commande = (new General()).find(new DetailCommande(),"DetailCommande",colone,values,"",false,(new DbConnect()).connect());
        DetailCommande[] dtCommande = new DetailCommande[commande.length];
        int i = 0;
        for(Object x:commande){
            dtCommande[i] = (DetailCommande) commande[i];
            i++;
        }
        PdfPTable table = new PdfPTable(4);
        PdfPCell nomMedic = new PdfPCell(new Phrase("Nom du médicament"));
        PdfPCell prixMedic = new PdfPCell(new Phrase("Prix du médicament"));
        PdfPCell nombreCommande = new PdfPCell(new Phrase("Nombre commandé"));
        PdfPCell total= new PdfPCell(new Phrase("Prix total"));
        table.addCell(nomMedic);
        table.addCell(prixMedic);
        table.addCell(nombreCommande);
        table.addCell(total);        
        table.setHeaderRows(1);
        double somme = 0;
        for (DetailCommande dtCommande1 : dtCommande) {
            table.addCell(dtCommande1.getNomProduit());
            table.addCell("" + dtCommande1.getPrixUnitaire());
            table.addCell("" + dtCommande1.getQuantiteProduit());
            double totalAffich = dtCommande1.getPrixUnitaire() * dtCommande1.getQuantiteProduit();
            somme+=totalAffich;
            table.addCell(""+totalAffich);
        }        
        document.add(table);
        ZapfDingbatsList zapfDingbatsList = new ZapfDingbatsList(43, 30);
        double prixService = 5000+(5*somme/100);
        zapfDingbatsList.add(new ListItem("Prix de nos services:"+prixService));
        prixService+=somme;
        zapfDingbatsList.add(new ListItem("Total à payer:"+prixService));
        zapfDingbatsList.add(new ListItem("Numero commande:"+this.commande.getIdCommande()));
        document.add(zapfDingbatsList);
    }
}
