/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author anjaratiana
 */
public class AccepterDemande {
    String idAccepterDemande;
    String idPharmacie;
    String idCommande;

    public AccepterDemande() {
    }

    public AccepterDemande(String idAccepterDemande, String idPharmacie, String idCommande) {
        this.idAccepterDemande = idAccepterDemande;
        this.idPharmacie = idPharmacie;
        this.idCommande = idCommande;
    }

    public String getIdAccepterDemande() {
        return idAccepterDemande;
    }

    public void setIdAccepterDemande(String idAccepterDemande) {
        this.idAccepterDemande = idAccepterDemande;
    }

    public String getIdPharmacie() {
        return idPharmacie;
    }

    public void setIdPharmacie(String idPharmacie) {
        this.idPharmacie = idPharmacie;
    }

    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }
    
    
}
