/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author anjaratiana
 */
public class CommandePrix extends Commande{
    double prix;

    public double getPrix() {
        return prix;
    }
    public CommandePrix(Commande a){
        this.setIdCommande(a.getIdCommande());
        this.setNomclient(a.getNomclient());
        this.setContact(a.getContact());
        this.setAdresse(a.getAdresse());
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

   
}
