/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author je sais pas
 */
public class Produit 
{
    String idproduit;
    String nomproduit;
    List<Produit> liste_produit;
    int quantite;
    
    public Produit()
    {
        
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    
    public void setQuantite(String quantiteproduit) 
    {
        int qte = Integer.parseInt(quantiteproduit);
        setQuantite(qte);
    }

    public Produit(String idproduit, String nomproduit, List<Produit> liste_produit, int quantite) {
        this.idproduit = idproduit;
        this.nomproduit = nomproduit;
        this.liste_produit = liste_produit;
        this.quantite = quantite;
    }
    
    

    public Produit(String idproduit, String nomproduit, String quantite) {
        this.idproduit = idproduit;
        this.nomproduit = nomproduit;
        setQuantite(quantite);
    }

    public String getIdproduit() 
    {
        return idproduit;
    }

    public void setIdproduit(String idproduit) 
    {
        this.idproduit = idproduit;
    }

    public String getNomproduit() 
    {
        return nomproduit;
    }

    public void setNomproduit(String nomproduit) 
    {
        this.nomproduit = nomproduit;
    }

    public List<Produit> getListe_produit() {
        return liste_produit;
    }

    public void setListe_produit(List<Produit> liste_produit) {
        this.liste_produit = liste_produit;
    }
    
    public List<Produit> ajouter_liste_produit(Produit p)
    {
        List<Produit> liste_p = new ArrayList<Produit>();
        liste_p.add(p);
        
        return liste_p;
    }
}
