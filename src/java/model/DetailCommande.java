/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.DbConnect;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author anjaratiana
 */
public class DetailCommande {
    String idDetailCommande;
    String idCommande;
    int quantiteProduit;
    String nomProduit;
    double prixUnitaire;
    
    public String getIdDetailCommande() {
        return idDetailCommande;
    }

    public void setIdDetailCommande(String idDetailCommande) {
        this.idDetailCommande = idDetailCommande;
    }

    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }

    public int getQuantiteProduit() {
        return quantiteProduit;
    }

    public void setQuantiteProduit(int quantiteProduit) {
        this.quantiteProduit = quantiteProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
    public void updateDtCommande(String[] idDetailCommande,double[] value) throws Exception {   
        Connection db = (new DbConnect()).connect();
        int count = idDetailCommande.length;
        try{
            for(int i = 0;i<count;i++){
                String query = "update DetailCommande set prixunitaire = "+value[i]+" where idDetailCommande like '"+idDetailCommande[i]+"'";
                PreparedStatement stmt = null;
                try {
                    stmt = db.prepareStatement(query);                    
                    int executeUpdate = stmt.executeUpdate();
                }
                catch(SQLException e) {
                    throw new Exception(""+stmt);
                }
                finally {
                    if(stmt!=null) {
                        stmt.close();
                    }
                }
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }
    }
    
}
