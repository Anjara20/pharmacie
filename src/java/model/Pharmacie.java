package model;

public class Pharmacie
{
    String idPharmacie;
    String nom;
    String contact;
    String lieu;
    String coordonnees;

    public Pharmacie() 
    {

    }

    public Pharmacie(String idPharmacie, String nom, String contact, String lieu, String coordonnees) 
    {
        this.setIdPharmacie(idPharmacie);
        this.setNom(nom);
        this.setContact(contact);
        this.setLieu(lieu);
        this.setCoordonnees(coordonnees);
    }

    public String getIdPharmacie() 
    {
        return this.idPharmacie;
    }

    public void setIdPharmacie(String idPharmacie) 
    {
        this.idPharmacie = idPharmacie;
    }

    public String getNom() 
    {
        return this.nom;
    }

    public void setNom(String nom) 
    {
        this.nom = nom;
    }

    public String getContact() 
    {
        return this.contact;
    }

    public void setContact(String contact) 
    {
        this.contact = contact;
    }

    public String getLieu() 
    {
        return this.lieu;
    }

    public void setLieu(String lieu) 
    {
        this.lieu = lieu;
    }

    public String getCoordonnees() 
    {
        return this.coordonnees;
    }

    public void setCoordonnees(String coordonnees) 
    {
        this.coordonnees = coordonnees;
    }
}