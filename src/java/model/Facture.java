package model;

import dao.DbConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Facture
{
    String idFacture;
    String idPharmacie;
    String idCommande;
    double prix;

    public Facture() 
    {

    }

    public Facture(String idPharmacie, double prix, String idCommande) {
        this.idPharmacie = idPharmacie;
        this.prix = prix;
        this.idCommande = idCommande;
    }
    public void insert_Facture() throws Exception{
        String query= "insert into Facture values('FAC'||nextval('seq_facture'),?,?,?)";
        Connection db = (new DbConnect()).connect();
        PreparedStatement stmt = null;
        try {
            stmt = db.prepareStatement(query);
            stmt.setString(1,this.getIdCommande());
            stmt.setString(2,this.getIdPharmacie());
            stmt.setDouble(3,this.getPrix());           
            System.out.println(stmt);
            int executeUpdate = stmt.executeUpdate();
        }
        catch(SQLException e) {
            throw new Exception(""+stmt);
        }
        finally {
            if(stmt!=null) {
                stmt.close();
            }
            db.close();
        }        
    }
    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }

    public String getIdFacture() 
    {
        return this.idFacture;
    }

    public void setIdFacture(String idFacture) 
    {
        this.idFacture = idFacture;
    }

    public String getIdPharmacie() 
    {
        return this.idPharmacie;
    }

    public void setIdPharmacie(String idPharmacie) 
    {
        this.idPharmacie = idPharmacie;
    }

    public double getPrix() 
    {
        return this.prix;
    }

    public void setPrix(double prix) 
    {
        this.prix = prix;
    }
}