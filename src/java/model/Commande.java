/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author anjaratiana
 */
public class Commande
{
    String idCommande;
    String ordonnance;
    String adresse;
    String contact;
    String quartier;
    String nomclient;
    int etat;
    String idLivreur;

    public Commande() {
    }

    public Commande(String idCommande, String ordonnance, String adresse, String contact, String quartier, String nomclient, int etat, String idLivreur) {
        this.idCommande = idCommande;
        this.ordonnance = ordonnance;
        this.adresse = adresse;
        this.contact = contact;
        this.quartier = quartier;
        this.nomclient = nomclient;
        this.etat = etat;
        this.idLivreur = idLivreur;
    }
     public Commande(String ordonnance,String adresse, String contact, String quartier, String nomclient, int etat, String idlivreur) 
    {
        this.setOrdonnance(ordonnance);
        setIdCommande("'com'||nextval('seq_commande')");
        setAdresse(adresse);
        setContact(contact);
        setQuartier(quartier);
        setNomclient(nomclient);
        setEtat(0);
        setIdLivreur(idlivreur);
    }

    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }

    public String getOrdonnance() {
        return ordonnance;
    }

    public void setOrdonnance(String ordonnance) {
        this.ordonnance = ordonnance;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getNomclient() {
        return nomclient;
    }

    public void setNomclient(String nomclient) {
        this.nomclient = nomclient;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(String idLivreur) {
        this.idLivreur = idLivreur;
    }
    
    
}

