create table Commande(
	IdCommande varchar(10),
	Ordonnance varchar(50),
	Adresse varchar(50),
	Contact varchar(20),
	Quartier GEOMETRY,
	NomClient varchar(50),
	Etat INTEGER,
	IdLivreur varchar(10),
	primary key (IdCommande)
);
create sequence seq_commande start with 1 increment by 1;

create table DetailCommande(
	IdDetailCommande varchar(10),
	IdCommande varchar(10),
	QuantiteProduit INTEGER,
	NomProduit varchar(50),
	PrixUnitaire DECIMAL(11,2),
	primary key (IdDetailCommande),
	foreign key (IdCommande) references Commande (IdCommande)
);
create sequence seq_detailcommande start with 1;

create table Produit(
	IdProduit varchar(10),
	NomProduit varchar(50),
	primary key (IdProduit)
);
create sequence seq_produit start with 1;
 
insert into Produit values
('pro'||nextval('seq_produit'), 'Halothane'),
('pro'||nextval('seq_produit'), 'Isoflurane'),
('pro'||nextval('seq_produit'), 'Oxygene'),
('pro'||nextval('seq_produit'), 'Ketamine'),
('pro'||nextval('seq_produit'), 'Propofol'),
('pro'||nextval('seq_produit'), 'Bupivacaine'),
('pro'||nextval('seq_produit'), 'Lidocainecon'),
('pro'||nextval('seq_produit'), 'Ephedrine'),
('pro'||nextval('seq_produit'), 'Atropine'),
('pro'||nextval('seq_produit'), 'Midazolam'),
('pro'||nextval('seq_produit'), 'Morphine'),
('pro'||nextval('seq_produit'), 'Ibuprofene '),
('pro'||nextval('seq_produit'), 'Paracetamol'),
('pro'||nextval('seq_produit'), 'Codeine'),
('pro'||nextval('seq_produit'), 'Morphine'),
('pro'||nextval('seq_produit'), 'Allopurinol'),
('pro'||nextval('seq_produit'), 'Amitriptyline'),
('pro'||nextval('seq_produit'), 'Cyclizine'),
('pro'||nextval('seq_produit'), 'Dexamethasone'),
('pro'||nextval('seq_produit'), 'Diazepam');
create table Pharmacie(
    idPharmacie varchar(50) primary key,
    nom varchar(50),
    contact varchar(50),
    lieu varchar(50),
    coordonnees geometry
);
create sequence seq_pharmacie;

create table AccepterDemande(
    idAccepterDemande varchar(50) primary key,
    idPharmacie varchar(50) references Pharmacie,
    idCommande varchar(50) references Commande
);
create sequence seq_accepterdemande;

create table Facture(
    idFacture varchar(50) primary key,
    idCommande varchar(50) references Commande,
    idPharmacie varchar(50) references Pharmacie,
    prix DECIMAL(11,2)
);
create sequence seq_facture start with 1 increment by 1;

create table Livreur(
    idLivreur varchar(50) primary key,
    nom varchar(50),
    contact varchar(50),
    adresse varchar(50),
    coordonnees geometry
);

create sequence seq_facture;
insert into Pharmacie values('PHA1','Metropole Antaninarenina','0321565478','Antaninarenina',ST_MakePoint(47.525952303410044,-18.910482248556512));
insert into Pharmacie values('PHA2','Tsaramasay','0325687452','Tsaramasay',ST_MakePoint(47.51603690555312,-18.893875328664706));
insert into Pharmacie values('PHA3','Analakely','0341565478','Analakely',ST_MakePoint(47.52610340537759,-18.90872071747558));
insert into Pharmacie values('PHA4','Ankadifotsy','0331565478','Ankadifotsy',ST_MakePoint(47.52557709072805,-18.89954081040379));

insert into Livreur values('LIV1','Liva Raseta','0322065874','Tsimbazaza',ST_MakePoint(47.52742986244185,-18.932667161680143));
insert into Livreur values('LIV2','Rabe Andria','0324586257','Tanjombato',ST_MakePoint(47.52835514338449,-18.95890764079767));
insert into Livreur values('LIV3','Faly Ranjeva','0322365874','Behoririka',ST_MakePoint(47.5243194885849,-18.902042086390075));
insert into Livreur values('LIV4','Andrianina Rado','0326378135','Andraharo',ST_MakePoint(47.50867084841963,-18.884014560873098));

create view ListeCommande as 
	select Commande.IdCommande,
			ordonnance,
			adresse,
			contact,
			quartier,
			nomClient,
			etat,
			idLivreur,
			IdDetailCommande,
			quantiteProduit,
			nomProduit
	from Commande, DetailCommande
	where detailCommande.idCommande = commande.idCommande;