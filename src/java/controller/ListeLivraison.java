/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import service.LivreurService;

/**
 *
 * @author anjaratiana
 */
public class ListeLivraison extends HttpServlet{

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        LivreurService call = new LivreurService();
        try {
            HttpSession session = request.getSession();
            request.setAttribute("commandes", call.getCommandeLivreur(session.getAttribute("livreur").toString()));
            this.getServletContext().getRequestDispatcher("/livraison.jsp").forward( request, response );
        } 
        catch (Exception e) {
            out.println(e);
        }
    }
    
}
