/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Commande;
import model.DetailCommande;
import service.CommandeService;

/**
 *
 * @author anjaratiana
 */
public class MyCommandeController extends HttpServlet{

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();        
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            out.println("Aucune demande...");
        }
        else{
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("IdCommande")) {
                    CommandeService call = new CommandeService();
                    try {
                        out.println(cookie.getValue());
                        Commande commande = call.getCommande("com"+cookie.getValue());
                        DetailCommande[] details = call.allComandefromDemande("com"+cookie.getValue());
                        request.setAttribute("details",details);
                        request.setAttribute("commande",commande);
                        this.getServletContext().getRequestDispatcher("/ma-demande.jsp").forward( request, response );
                    } catch (Exception ex) {
                        out.println(ex);
                    }
                }
            }
        }
        
    }
    
}
