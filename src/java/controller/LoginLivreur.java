/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import service.LivreurService;

/**
 *
 * @author je sais pas
 */
public class LoginLivreur extends HttpServlet
{
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        response.sendRedirect("login-livreur.jsp");
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();  
        out.println("ato");
        try
        {
            LivreurService livreur = new LivreurService();
            String idLivreur = livreur.connect(request.getParameter("nom"));
            if(idLivreur!=null){
                HttpSession session = request.getSession();
                session.setAttribute("livreur",idLivreur);
                response.sendRedirect("./livrer");
            }
            throw new Exception("Ce livreur n\'existe pas");
            
        }
        catch(Exception ex)
        {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('"+ex.getMessage()+"');");
            out.println("location='./index.jsp';");
            out.println("</script>");
        }
    }
}
