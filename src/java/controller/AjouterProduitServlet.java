/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.sun.media.sound.InvalidFormatException;
import dao.General;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Commande;
import model.Produit;
import service.CommandeService;
import service.DetailCommandeService;

/**
 *
 * @author je sais pas
 */
public class AjouterProduitServlet extends HttpServlet
{
    List<Produit> liste = new ArrayList<Produit>();
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        //List<Cookie> liste = ArrayList<Cookie>();
        String quantite = request.getParameter("quantite");
        String nomproduit = request.getParameter("pdt");
        String idproduit = request.getParameter("idpdt");
        Produit p = new Produit(idproduit, nomproduit, quantite);
        this.liste.add(p);
        //HttpSession session = request.getSession();
        //session.setAttribute("id", id);
        response.sendRedirect("CommandeView.jsp");
    }
}
