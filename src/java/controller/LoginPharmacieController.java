/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.PharmacieDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Pharmacie;
import service.LivreurService;

/**
 *
 * @author je sais pas
 */
public class LoginPharmacieController extends HttpServlet
{
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        try
        {
            if(request.getParameter("nom") != null)
            {
                String nompharmacie = request.getParameter("nom");
                Pharmacie p = (new PharmacieDao()).getLogin(nompharmacie);
                HttpSession session = request.getSession();
                session.setAttribute("pharmacie", p.getIdPharmacie());
                response.sendRedirect("./listeDemande");
            }
            throw new Exception("Cette pharmacie est introuvable");            
        }
        catch(Exception ex)
        {
            out.println("<script type=\"text/javascript\">");
            out.println("alert('"+ex.getMessage()+"');");
            out.println("location='./login.jsp';");
            out.println("</script>");
        }
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        response.sendRedirect("LoginPharmacie.jsp");
    }
}