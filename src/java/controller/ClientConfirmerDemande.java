/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import model.PDF1;
import service.CommandeService;

/**
 *
 * @author anjaratiana
 */
public class ClientConfirmerDemande extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();          
        try {
            out.println("<script type=\"text/javascript\">");
            out.println("if(confirm('Etes-vous sûr de continuer?')){");
            out.println("} else {");
            out.println("location='./demande-attente';");
            out.println("}"); 
            out.println("</script>");
            CommandeService a = new CommandeService();
            int modifier = a.confirmerCommande(request.getParameter("idCommande").trim(), request.getParameter("answer"));
            response.sendRedirect("ModeleClient.jsp");
        }
        catch(Exception e) {
            out.println(e);
        }
    }    
}
