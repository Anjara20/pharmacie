/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.sun.media.sound.InvalidFormatException;
import dao.General;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.CommandeService;
import service.DetailCommandeService;
import model.Commande;
import model.Produit;

/**
 *
 * @author je sais pas
 */
public class CommandeServlet extends HttpServlet
{
    List<Produit> liste = new ArrayList<Produit>();
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        PrintWriter out = response.getWriter();      
        try 
        {
            String idcmd = (new General()).getCurrval(null, "commande", 1);
            Cookie cookie = new Cookie("IdCommande", idcmd);
            response.addCookie(cookie);
            String contact = request.getParameter("contact");
            String quartier = request.getParameter("quartier");
            String nomclient = request.getParameter("nomclient");
            String idlivreur = "l";
            String adresse = request.getParameter("adresse");
            Commande cmd = new Commande("",adresse, contact, quartier, nomclient, 0, idlivreur);
            List<DetailCommandeService> liste_det_cmd = new ArrayList<DetailCommandeService>();
            int taille = this.liste.size();
            for(int i = 0; i < taille; i++)
            {
                DetailCommandeService dcmd = new DetailCommandeService(idcmd, liste.get(i).getQuantite(), liste.get(i).getNomproduit(), 0);
                liste_det_cmd.add(dcmd);
            }
            (new CommandeService()).save_commande(cmd, liste_det_cmd);
            this.liste.clear();
            response.sendRedirect("CommandeView.jsp");
        } 
        catch(NumberFormatException i)
        {
            request.setAttribute("erreur", "Format invalide ou champ null");
            RequestDispatcher disp = request.getRequestDispatcher("CommandeView.jsp");
            disp.forward(request, response);
        }
        catch (Exception ex) 
        {
            request.setAttribute("erreur", ex.getMessage());
            RequestDispatcher disp = request.getRequestDispatcher("CommandeView.jsp");
            disp.forward(request, response);
        }
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        String quantite = request.getParameter("quantite");
        String nomproduit = request.getParameter("pdt");
        System.out.println("nomproduit = " + nomproduit);
        String idproduit = request.getParameter("idpdt");
        Produit p = new Produit(idproduit, nomproduit, quantite);
        this.liste.add(p);
        int taille = this.liste.size();
        for(int i = 0; i < taille; i++)
        {
            System.out.println(this.liste.get(i).getNomproduit());
        }
        response.sendRedirect("CommandeView.jsp");
    }
}
