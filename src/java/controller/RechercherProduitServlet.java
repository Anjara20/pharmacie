/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Produit;
import service.ProduitService;

/**
 *
 * @author je sais pas
 */
public class RechercherProduitServlet extends HttpServlet
{
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        try
        {
            String motcle = request.getParameter("produit");
            List<Produit> tab = (new ProduitService()).recherche(motcle, null);
            request.setAttribute("produits", tab);
            RequestDispatcher dispatch = request.getRequestDispatcher("CommandeView.jsp");
            dispatch.forward(request, response);
        }
        catch(Exception ex)
        {
            request.setAttribute("erreur", ex.getMessage());
            RequestDispatcher disp = request.getRequestDispatcher("CommandeView.jsp");
            disp.forward(request, response);
            //throw new ServletException(ex);
        }
    }
}
