/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.PDF1;

/**
 *
 * @author anjaratiana
 */
public class AccepterDemandeController extends HttpServlet{

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();          
        try {
            String idCommande = request.getParameter("idCommande");
            PDF1 facturer = new PDF1(idCommande,"D:\\e-pharmacie\\web\\pdf\\facture"+idCommande+".pdf");  
            File f = new File("D:\\e-pharmacie\\web\\pdf\\facture"+idCommande+".pdf");
            if(f.isFile())
            { 
                request.setAttribute("pdf","./pdf/facture"+idCommande+".pdf");
                this.getServletContext().getRequestDispatcher("/facture.jsp").forward( request, response );
            }
            
        }
        catch(Exception e) {
            out.println(e);
            out.println("<script type=\"text/javascript\">");
            out.println("alert('"+e.getMessage()+"');");
            out.println("location='./index.jsp';");
            out.println("</script>");
        }
    }
}
