/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.DetailCommande;
import model.Facture;
import service.CommandeService;
import service.DetailCommandeService;

/**
 *
 * @author anjaratiana
 */
public class FormerFacture extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        PrintWriter out = response.getWriter();     
        HttpSession session = request.getSession();
         if(request.getParameter("insertFacture")!=null){
            Double total=Double.parseDouble(request.getParameter("total"));
            String idCommande=request.getParameter("idCommande");
            String idPharmacie=session.getAttribute("pharmacie").toString();
            Facture fact=new Facture(idPharmacie,total,idCommande);
            try {
                fact.insert_Facture();
                int nbFanafody=Integer.parseInt(request.getParameter("nbFanafody"));
                double[] pu= new double[nbFanafody];
                String[] idDetailCommande=new String[nbFanafody];
                for(int i=0;i<nbFanafody;i++){
                    idDetailCommande[i]=request.getParameter("idDetailCommande"+(i+1));
                    pu[i]=Double.parseDouble(request.getParameter("prixUnitaire"+(i+1)));
                }
                DetailCommande call = new DetailCommande();
                call.updateDtCommande(idDetailCommande,pu);
                CommandeService.accordPharmacie(idCommande,"PHA1");
            } catch (Exception ex) {
                out.println(ex);
            }
            response.sendRedirect("./listeDemande");
        }
        else{
            String idCommande=request.getParameter("idCommande");
            DetailCommande[] dtcmd;
            try {
                dtcmd = (new CommandeService()).allComandefromDemande(idCommande);
                request.setAttribute("DetailCommandes", dtcmd);
                RequestDispatcher rd = request.getRequestDispatcher("formulaire.jsp");
                rd.forward(request, response);
            } catch (Exception ex) {
                out.println(ex);
            }
           
        }
       
    }
}
