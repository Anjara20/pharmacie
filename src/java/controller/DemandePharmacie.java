package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import service.PharmacieService;
import model.ListeCommande;

public class DemandePharmacie extends HttpServlet
{
    public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
    {
        PrintWriter out = response.getWriter();  
        try
        {
            String idPharmacie = "PHA1";
            ListeCommande[] commandes = new PharmacieService().getDemande(idPharmacie);
            request.setAttribute("commandes", commandes);
            RequestDispatcher rd = request.getRequestDispatcher("listeCommande.jsp");
            rd.forward(request, response);
        }
        catch(Exception ex)
        {
            out.println(ex);
        }
    }
    
}