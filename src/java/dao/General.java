/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author je sais pas
 */
public class General 
{
    public void insert(Object o, String nomTable) throws Exception, SQLException 
    {
        @SuppressWarnings("resource")
        Connection con=null;
        try 
        {
            con = new DbConnect().connect();
            con.setAutoCommit(false);
            insert(o,nomTable,con);
            con.commit();
        } 
        catch (Exception e) 
        {
            con.rollback();
            throw e;
        }
        finally 
        {
            if(con!=null) 
            {
                    con.close();
            }
        }
    }
    public void insert(Object o, String nomTable, Connection con) throws Exception, SQLException 
    {
        @SuppressWarnings("resource")
        PreparedStatement req = null;
        try 
        {
            Class<? extends Object> c = o.getClass();
            Field[] f = c.getDeclaredFields();
            String request = "insert into " + nomTable + "(";
            for (int i = 0; i < f.length; i++) 
            {
                if (i != f.length - 1)
                    request += f[i].getName() + ",";
                else
                    request += f[i].getName();
            }
            request += ")values(";
            Object[] valeurs = new Object[f.length];
            for (int i = 0; i < f.length; i++) 
            {
                valeurs[i] = c.getDeclaredMethod("get" + this.toUpCase(f[i].getName(), "up"), new Class[0]).invoke(o);
            }
            
            int nb_parametre = 0;
            for (int i = 0; i < valeurs.length; i++) 
            {
                if (valeurs[i] instanceof String && ((String) valeurs[i]).contains("nextval")) 
                {
                    request += (String) valeurs[i] + ",";
                } 
                else 
                {
                    if (f[i].getName().equals("quartier"))
                    {
                        request += "ST_MakePoint(" + valeurs[i] + ")";
                        if (i != valeurs.length - 1)
                            request += ",";
                    }
                    else if (i != valeurs.length - 1)
                    {
                        request += "?,";
                        nb_parametre ++;
                    }
                    else
                    {
                        request += "?";
                        nb_parametre ++;
                    }
                }
            }
            request += ")";
            System.out.println(request);
            req = con.prepareStatement(request);
            
            int indice = 1;
            for (int i = 0; i < valeurs.length; i++) 
            {
                System.out.println(valeurs[i].toString());
                if (valeurs[i] instanceof String && ((String) valeurs[i]).contains("nextval")) 
                {
                    
                }
                else if (valeurs[i] instanceof String) 
                {
                    if(!f[i].getName().equals("quartier"))
                    {
                        req.setString(indice, (String) valeurs[i]); 
                        indice ++;
                    }
                } 
                else if (valeurs[i] instanceof Date) 
                {
                    req.setDate(indice, (Date) valeurs[i]); 
                    indice ++;
                } 
                else if (valeurs[i] instanceof Double) 
                {
                    req.setDouble(indice, (Double) valeurs[i]); 
                    indice ++;
                } 
                else if (valeurs[i] instanceof Integer) 
                {
                    req.setInt(indice, (Integer) valeurs[i]); 
                    indice ++;
                }
            }
            req.executeUpdate();
            System.out.println("INSEREE");
        } 
        catch (Exception e) 
        {
            throw e;
        } 
        finally 
        {
            req.close();
        }
    }
    public Object[] find(Object o, String nomTable, String[] where, Object[] valeurs,String afterWhere,Boolean isHerited)
                    throws SQLException, Exception 
    {
        @SuppressWarnings("resource")
        Connection con=null;
        try 
        {
            con = new DbConnect().connect();
            Object[] res=find(o,nomTable,where,valeurs,afterWhere,isHerited,con);
            return res;
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally 
        {
            if(con!=null) 
            {
                con.close();
            }
        }
    }
    @SuppressWarnings("resource")
    public Object[] find(Object o, String nomTable, String[] where, Object[] valeurs,String afterWhere,Boolean isHerited, Connection con)
                    throws SQLException, Exception 
    {
        Object resultFinal = null;
        ResultSet result = null;
        PreparedStatement req = null;
        try 
        {
            Class<? extends Object> classeO = o.getClass();
            Field[] f;
            Method[] tabmethSet;
            if(isHerited) 
            {
                Field[]superclassF=classeO.getSuperclass().getDeclaredFields();
                Field[] classF=classeO.getDeclaredFields();
                f=new Field[superclassF.length+classF.length];
                System.out.println(f.length);
                for(int i=0;i<superclassF.length;i++) 
                {
                    f[i]=superclassF[i];	
                }
                for(int i=superclassF.length,j=0;j<classF.length;j++) 
                {
                    f[i]=classF[j];	
                    i++;
                }
                Method[] tabmethSetsuperclass=new Method[superclassF.length];
                Method[] tabmethSetclass=new Method[classF.length];
                for (int i = 0; i < superclassF.length; i++) 
                {
                    tabmethSetsuperclass[i] = classeO.getSuperclass().getDeclaredMethod("set" + this.toUpCase(superclassF[i].getName(), "up"), superclassF[i].getType());
                }
                for (int i = 0; i < classF.length; i++) 
                {
                    tabmethSetclass[i] = classeO.getDeclaredMethod("set" + this.toUpCase(classF[i].getName(), "up"), classF[i].getType());
                }
                tabmethSet=new Method[tabmethSetsuperclass.length+tabmethSetclass.length];
                for(int i=0;i<tabmethSetsuperclass.length;i++) 
                {
                    tabmethSet[i]=tabmethSetsuperclass[i];	
                }
                for(int i=tabmethSetsuperclass.length,j=0;j<tabmethSetclass.length;j++) 
                {
                    tabmethSet[i]=tabmethSetclass[j];	
                    i++;
                }
            }
            else 
            {
                f=classeO.getDeclaredFields();
                tabmethSet = new Method[f.length];

                for (int i = 0; i < f.length; i++) 
                {
                    tabmethSet[i] = classeO.getDeclaredMethod("set" + this.toUpCase(f[i].getName(), "up"), f[i].getType());
                }
            }


            String request = "";
            if (where.length == 0) 
            {
                request += "select * from " + nomTable;
            } 
            else 
            {
                request += "select * from " + nomTable + " where ";
            }
            for (int i = 0; i < where.length; i++) 
            {
                if (i != where.length - 1)
                    request += where[i] + "=? and ";
                else
                    request += where[i] + "=?";
            }
            request+=afterWhere;
            System.out.println(request);
            req = con.prepareStatement(request, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < valeurs.length; i++) 
            {
                if (valeurs[i] instanceof String) 
                {
                    req.setString(i + 1, (String) valeurs[i]);
                } 
                else if (valeurs[i] instanceof Date) 
                {
                    req.setDate(i + 1, (Date) valeurs[i]);
                } 
                else if (valeurs[i] instanceof Double) 
                {
                    req.setDouble(i + 1, (Double) valeurs[i]);
                } 
                else if (valeurs[i] instanceof Integer) 
                {
                    req.setInt(i + 1, (Integer) valeurs[i]);
                }
            }
            System.out.println(req);
            result = req.executeQuery();

            int taille = 0;
            result.last();
            taille = result.getRow();
            result.beforeFirst();

            resultFinal = Array.newInstance(classeO, taille);
            int a = 0;
            while (result.next()) 
            {
                Object temp = classeO.getDeclaredConstructor().newInstance();
                for (int i = 0; i < f.length; i++) 
                {
                    System.out.println(f[i].getName());
                    if (f[i].getType().getName().equals("double")) 
                    {
                        tabmethSet[i].invoke(temp, result.getDouble(i + 1));
                    } 
                    else if (f[i].getType().getName().equals("int")) 
                    {
                        tabmethSet[i].invoke(temp, result.getInt(i + 1));
                    } 
                    else if (f[i].getType().getName().equals("java.sql.Date")) 
                    {
                        tabmethSet[i].invoke(temp, result.getDate(i + 1));
                    } 
                    else 
                    {
                        tabmethSet[i].invoke(temp, result.getString(i + 1));
                    }
                }
                Array.set(resultFinal, a, temp);
                a++;
            }

            return (Object[]) resultFinal;
        } 
        catch (Exception e) 
        {
            throw e;
        } 
        finally 
        {
            if (result != null)result.close();
            if (req != null)req.close();
        }
    }
    
    public String getCurrval(Connection c, String nomtable, int etat) throws Exception
    {
        String valiny = "";
        Statement req = null;
        ResultSet res=null;
        int rep = 0;
        try
        {
            if(c==null && etat != 0)
            {
                DbConnect db=new DbConnect();
                c=db.connect();
            }
            
            req=c.createStatement();
            String sql = "SELECT nextval('seq_" + nomtable +"')";
            res=req.executeQuery(sql);

            while(res.next())
            {
                rep = res.getInt("nextval") + 1;
                valiny = Integer.toString(rep);
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(c != null && etat != 0)
                c.close();
            res.close();
            req.close();
        }
        return valiny;
    }
    
    public String toUpCase(String s, String type) 
    {
        char debut = s.charAt(0);
        String premier = String.valueOf(debut);
        String finprem = new String();
        if (type.equals("up"))
            finprem = premier.toUpperCase();
        else if (type.equals("down"))
            finprem = premier.toLowerCase();

        String deuxieme = s.substring(1);
        String fin = finprem + deuxieme;

        return fin;
    }
    public Date addSubtractDaysFromDate(Date dt, long numberOfDay, int type) 
    {
            // LocalDate lit=LocalDate.of(2019,05,30);
            // if type=0 add else if type=1 subtract
        LocalDate lcdt = dt.toLocalDate();
        Date newDate = new Date(new java.util.Date().getTime());
        if (type == 0) 
        {
            newDate = Date.valueOf(lcdt.plusDays(numberOfDay).toString());
        } 
        else 
        {
            newDate = Date.valueOf(lcdt.minusDays(numberOfDay).toString());
        }
        return newDate;
    }
    public int getDaysBetweenTowDates(Date d1, Date d2) 
    {
        int nombreJours = 0;
        long diff = d1.getTime() - d2.getTime();
        nombreJours = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        return nombreJours;
    }
//		  public static void main(String[] args) {
//		  
//		  /*SimpleDateFormat form=new SimpleDateFormat("yyyy-MM-dd");
//		  
//		  @SuppressWarnings("unused") Date d1=null; Date d3=null; try { java.util.Date
//		  d11=form.parse("2019-05-30"); java.util.Date d31=form.parse("2019-06-01");
//		  d1=new Date(d11.getTime()); d3=new Date(d31.getTime()); } catch
//		  (ParseException e) { // TODO Auto-generated catch block e.printStackTrace();
//		  }
//		  
//		  System.out.println("local="+addSubtractDaysFromDate(d3,2,1));
//		  
//		  //Date d=new Date(new java.util.Date().getTime()); //java.util.Date d2=new
//		  java.util.Date(); System.out.println(generateRandomMdp(8));
//		  System.out.println(generateRandomIdentifier(30));*/
//		  
//		  Test test=new Test("nextval('seq_test')",new Date(new java.util.Date().getTime()),5999.0,1,"nom"); 
//		  Connection con=null;
//		  try {
//			  con=connect();
//			  con.setAutoCommit(false);
//			  insert(test,"test",con); 
//			  Test[]testTab=(Test[])find(test,"test",new String[] {"test1","test4"},new Object[] {"2",1},con);
//			  con.commit();
//			  System.out.println(testTab[0].getTest2().toString()); 
//		  }
//		  catch (Exception e) {
//			 try {
//				 if(con!=null)con.rollback();
//			} catch (SQLException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		   e.printStackTrace(); 
//		  } finally {
//			  try {
//					 if(con!=null) con.close();
//				} catch (SQLException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//			 
//		  }
//		}
}
