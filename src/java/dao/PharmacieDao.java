package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Pharmacie;
import java.util.List;
import java.util.ArrayList;
import model.ListeCommande;

public class PharmacieDao
{
    public Pharmacie getPharmacie(String idPharmacie, Connection c) throws Exception
    {
        ResultSet res = null;
        PreparedStatement statement = null;
        List<ListeCommande> list = new ArrayList();
        try
        {
            Pharmacie pharmacie = null;
            String sql = "select idPharmacie, nom, contact, lieu, ST_AsText(coordonnees) as coordonnees from Pharmacie where idPharmacie=?";
            statement = c.prepareStatement(sql);
            statement.setString(1, idPharmacie);
            res = statement.executeQuery();
            while(res.next())
            {
                pharmacie = new Pharmacie(res.getString("idPharmacie"), res.getString("nom"), res.getString("contact"), res.getString("lieu"), res.getString("coordonnees"));
            }
            return pharmacie;
        }
        catch(Exception e)
        {
            throw e;
        }   
    }

    public Pharmacie getLogin(String nom) throws Exception
    {
        ResultSet res = null;
        PreparedStatement statement = null;
        List<Pharmacie> list = new ArrayList();
        Connection c = new DbConnect().connect();
        try
        {
            Pharmacie pharmacie = null;
            String sql = "select idPharmacie, nom, contact, lieu,ST_AsText (ST_Transform (ST_SetSRID(coordonnees,4326), 4326)) as coordonnees from Pharmacie where nom=?";
            statement = c.prepareStatement(sql);
            statement.setString(1, nom);
            res = statement.executeQuery();
            while(res.next())
            {
                list.add(new Pharmacie(res.getString("idPharmacie"), res.getString("nom"), res.getString("contact"), res.getString("lieu"), res.getString("coordonnees")));
            }
            if(list.isEmpty())
            {
                throw new Exception("Cette pharmacie n'existe pas");
            }
            return list.get(0);
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            c.close();
        }    
    }

    /*public String getCercle(String idCommande) throws Exception
    {
        ResultSet res = null;
        PreparedStatement statement = null;
        Connection c = new Dbconnect().connect();
        try
        {
            String result = "";
            Object[] demandes = new General().find(new Object(), "Commande", new String[]{"idCommande"}, new Object[]{idCommande}, "", false);
            String sql = "select ST_AsText(ST_Buffer(commande.coordonees,100)) as cercle";
            res = statement.executeQuery();
            while(res.next())
            {
                result = res.getString("cercle");
            }
            return result;
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            c.close();
        }
    }*/
}