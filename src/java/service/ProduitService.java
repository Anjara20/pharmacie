/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.DbConnect;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Produit;

/**
 *
 * @author je sais pas
 */
public class ProduitService 
{
    public List<Produit> recherche(String motcle, Connection c)throws Exception
    {
        boolean isNull=false;
        if(c==null)
        {
            DbConnect db=new DbConnect();
            c=db.connect();
            isNull=true;
        }

        Statement req = null;
        ResultSet res=null;
        String sql="select * from produit where nomproduit like '%" + motcle + "%'";
        System.out.println("sql="+sql);
        req = c.createStatement();
        res = req.executeQuery(sql);
        
        List<Produit> rep = new ArrayList<Produit>();

        try
        {  
            while(res.next())
            {
                Produit p = new Produit();
                p.setIdproduit(res.getString("idproduit"));
                p.setNomproduit(res.getString("nomproduit"));
                rep.add(p);
            }
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

        finally
        {
            if(isNull==true)
            {
                c.close();
            }
            req.close();
            res.close();
        }
        return rep;
    }
}
