/*a
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.DbConnect;
import dao.General;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Commande;
import model.CommandePrix;
import model.DetailCommande;
import model.Facture;

/**
 *
 * @author anjaratiana
 */
public class LivreurService {
    public CommandePrix[] getCommandeLivreur(String idLivreur) throws Exception{
        Connection db = (new DbConnect()).connect();
        try{
            Commande[] commandes = CommandeService.selectCommande("idLivreur like '"+idLivreur+"'", db);
            int nombreVoulu = commandes.length;
            CommandePrix[] cmds = new CommandePrix[nombreVoulu];
            double[] prix = new double[nombreVoulu];
            for(int i = 0;i<nombreVoulu;i++){
                String[] colone = {"idCommande"};
                String[] values = {commandes[i].getIdCommande()};
                Object[] facture = (new General()).find(new Facture(),"Facture",colone,values,"",false,db);
                cmds[i] = new CommandePrix(commandes[i]);
                cmds[i].setPrix(((Facture) facture[0]).getPrix());
            }
            return cmds;
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }      
    }
    public String getURL(String idCommande) throws Exception{
        Connection db = (new DbConnect()).connect();
        try{
            String query = "select ST_AsText (ST_Transform (ST_SetSRID(quartier,4326), 4326)) as quartier,ST_AsText (ST_Transform (ST_SetSRID(Pharmacie.coordonnees,4326), 4326)) as coordonnees from Commande join AccepterDemande on Commande.idCommande = AccepterDemande.idCommande join Pharmacie on AccepterDemande.idPharmacie = Pharmacie.idPharmacie where Commande.idCommande like '"+idCommande+"'";
            PreparedStatement stmt = null;
            ResultSet resultRequest = null;
            String url = "";
            try {
                stmt = db.prepareStatement(query);
                resultRequest = stmt.executeQuery();
                while(resultRequest.next()) {
                    url= "?client="+resultRequest.getString("quartier")+"&&pharmacie="+resultRequest.getString("coordonnees");
                }
                return url;
            }
            catch(SQLException e) {
                throw e;
            }
            finally {
                if(resultRequest!=null) {
                    resultRequest.close();
                }
                if(stmt!=null) {
                    stmt.close();
                }
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }      
    }
    public String connect(String nomLivreur) throws Exception{
        Connection db = (new DbConnect()).connect();
        try
        {
            return this.getIdLivreur(nomLivreur, db);            
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            db.close();
        }    
    }
    public String getIdLivreur(String nomLivreur, Connection c) throws Exception
    {
        ResultSet res = null;
        PreparedStatement statement = null;
        String rep = null;
        try
        {
            String sql = "select idLivreur from Livreur where nom = ?";
            statement = c.prepareStatement(sql);
            statement.setString(1, nomLivreur);
            res = statement.executeQuery();
            while(res.next())
            {
                rep = res.getString("idlivreur");
            }
            return rep;
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            c.close();
        }    
    }
}
