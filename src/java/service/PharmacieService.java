package service;

import dao.DbConnect;
import dao.General;
import model.AccepterDemande;
import model.ListeCommande;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import dao.PharmacieDao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Pharmacie;

public class PharmacieService
{
    public ListeCommande[] getDemande(String idPharmacie) throws Exception
    {
        ResultSet res = null;
        PreparedStatement statement = null;
        Connection c = new DbConnect().connect();
        General general = new General();
        List<ListeCommande> list = new ArrayList<ListeCommande>();
        try
        {
            Pharmacie pharmacie = new PharmacieDao().getPharmacie(idPharmacie, c);
            String sql = "select * from ListeCommande where ST_Intersects(ST_Buffer(quartier,1000), ST_GeomFromText('"+pharmacie.getCoordonnees()+"')) and etat=0";
            System.out.println(sql);
            statement = c.prepareStatement(sql);
            res = statement.executeQuery();
            while(res.next())
            {
                list.add(new ListeCommande(res.getString("idCommande"), res.getString("ordonnance"), res.getString("adresse"), 
                 res.getInt("contact"), res.getString("quartier"), res.getString("nomClient"), res.getInt("etat"), 
                 res.getString("idLivreur"), res.getString("idDetailCommande"), res.getInt("quantiteProduit"),
                 res.getString("nomProduit")));
            }
            ListeCommande[] result = new ListeCommande[list.size()];
            for(int i = 0;i<result.length;i++){
                result[i] = list.get(i);
            }
            return result;
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            c.close();
        }       
    }
}