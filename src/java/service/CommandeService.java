/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import dao.DbConnect;
import dao.General;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.AccepterDemande;
import model.Commande;
import model.DetailCommande;

/**
 *
 * @author je sais pas
 */
public class CommandeService 
{
    public void insert_commande(Commande cmd, Connection c) throws Exception
    {
        try
        {
            (new General()).insert(cmd, "commande", c);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public void save_commande(Commande cmd, List<DetailCommandeService> dcmd)throws Exception
    {
        DbConnect dbc = new DbConnect();
        Connection c = null;
        
        try
        {
            c = dbc.connect();
            c.setAutoCommit(false);
            insert_commande(cmd, c);
            String idcmd = (new General()).getCurrval(c, "commande", 0);
            int taille = dcmd.size();
            System.out.println("taille = " + taille);
            for(int i = 0; i < taille; i++)
            {
                dcmd.get(i).insert_detail_commande(c);
            }
            c.commit();
        }
        catch(Exception e)
        {
            c.rollback();
            throw e;
        }
        finally
        {
            if(c != null)
            {
                c.close();
            }
        }
    }
    public DetailCommande[] allComandefromDemande(String idDemande) throws Exception{
            String[] colone = {"idCommande"};
            String[] values = {idDemande};
            Object[] commande = (new General()).find(new DetailCommande(),"DetailCommande",colone,values,"",false,(new DbConnect()).connect());
            DetailCommande[] result = new DetailCommande[commande.length];
            int i = 0;
            for(Object x:commande){
                result[i] = (DetailCommande) commande[i];
                i++;
            }
            return result;
    }
    public static Commande[] selectCommande(String condition,Connection db) throws Exception{
        String query = "select idCommande,ordonnance,adresse,contact,ST_AsText (ST_Transform (ST_SetSRID(quartier,4326), 4326)) as quartier,nomClient,etat,idLivreur FROM commande";
        if(condition!=null) {
            query+=" where "+condition;
        }
        PreparedStatement stmt = null;
        ResultSet resultRequest = null;
        try {
            stmt = db.prepareStatement(query);
            resultRequest = stmt.executeQuery();
            ArrayList<Commande> result = new ArrayList<Commande>();
            int count = 0;
            while(resultRequest.next()) {
                Commande toGet = new Commande(resultRequest.getString("idCommande"),resultRequest.getString("ordonnance"),resultRequest.getString("adresse"),resultRequest.getString("contact"),resultRequest.getString("quartier"),resultRequest.getString("nomClient"),resultRequest.getInt("etat"),resultRequest.getString("idLivreur"));
                result.add(toGet);
                count++;
            }
            Commande[] getTheResult = new   Commande[count];
            for(int i = 0;i<count;i++) {
                getTheResult[i] = result.get(i);
            }
            return getTheResult;
        }
        catch(SQLException e) {
            throw new Exception(query);
        }
        finally {
            if(resultRequest!=null) {
                resultRequest.close();
            }
            if(stmt!=null) {
                stmt.close();
            }
        }
    }
    public static void updateCommande(Commande commandeModifier,Connection db,String nomColone,Object value) throws Exception {        
        String query = "update commande set %s = ? where idCommande like ?";
        query = String.format(query, nomColone);
        PreparedStatement stmt = null;
        try {
            stmt = db.prepareStatement(query);
            Field aModifier = Class.forName("model.Commande").getDeclaredField(nomColone);
            if(value instanceof String) {
                stmt.setString(1,value.toString());
            }
            if(value instanceof Integer) {
                stmt.setInt(1,(Integer) value);
            }
            if(value instanceof Float) {
                stmt.setFloat(1,(Float) value);
            }
            stmt.setString(2,commandeModifier.getIdCommande());
            int executeUpdate = stmt.executeUpdate();
        }
        catch(SQLException e) {
            throw new Exception(""+stmt);
        }
        finally {
            if(stmt!=null) {
                stmt.close();
            }
        }
    }
    public static void accordPharmacie(String idCommande,String idPharmacie) throws Exception{
        Connection db = (new DbConnect()).connect();
        try{
            Commande modifier = new Commande();
            modifier.setIdCommande(idCommande);
            CommandeService.updateCommande(modifier,db,"etat",11);
            AccepterDemande inserer = new AccepterDemande("'ACC'||nextval('seq_accepterDemande')",idPharmacie,idCommande);
            (new General()).insert(inserer, "AccepterDemande");
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }
    }
    public void accepterCommande(String idCommande,Connection db) throws Exception{
        Commande[] commande = this.selectCommande("idCommande like '"+idCommande+"'",db);
        if(commande.length==0){
            throw new Exception("Commande introuvable...");            
        }
        CommandeService.updateCommande(commande[0],db,"etat",21);
        CommandeService.updateCommande(commande[0],db,"idLivreur",this.getLivreurProche(idCommande, db));
    }
    public void annulerCommande(String idCommande,Connection db) throws Exception{
        Commande[] commande = this.selectCommande("idCommande like '"+idCommande+"'",db);
        if(commande.length==0){
            throw new Exception("Commande introuvable...");            
        }
        CommandeService.updateCommande(commande[0],db,"etat",31);
    }
    public int confirmerCommande(String idCommande,String reponse) throws Exception{
        Connection db = (new DbConnect()).connect();
        try{
            if(reponse.equals("accepter")){
                this.accepterCommande(idCommande, db);
                return 1;
            }
            else{
                this.annulerCommande(idCommande, db);
                return 0;
            }
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }
    }
    public String getLivreurProche(String idCommande,Connection db) throws Exception{
        String[] colone = {"idCommande"};
        String[] values = {idCommande};
        Object[] getReponse  = (new General()).find(new AccepterDemande(),"AccepterDemande",colone ,values,"",false);
        if(getReponse.length==0){
            throw new Exception("Réponse introuvable...");            
        }
        AccepterDemande reponse = (AccepterDemande) getReponse[0];
        String query = "select min(ST_DISTANCE(Pharmacie.coordonnees,Livreur.coordonnees)) as distance,idLivreur from Pharmacie,Livreur where idPharmacie like '"+reponse.getIdPharmacie()+"' group by idLivreur order by distance limit 1";
        PreparedStatement stmt = null;
        ResultSet resultRequest = null;
        String idLivreur = "";
        try {
            stmt = db.prepareStatement(query);
            resultRequest = stmt.executeQuery();
            while(resultRequest.next()) {
                idLivreur = resultRequest.getString("idLivreur");
            }
            return idLivreur;
        }
        catch(SQLException e) {
            throw new Exception(query);
        }
        finally {
            if(resultRequest!=null) {
                resultRequest.close();
            }
            if(stmt!=null) {
                stmt.close();
            }
        }
    }
    public Commande getCommande(String idCommande) throws Exception{
        Connection db = (new DbConnect()).connect();
        try{
            Commande[] commandes = CommandeService.selectCommande("idCommande like '"+idCommande+"'", db);
            if(commandes.length==0) {
                throw new Exception("Commande introuvable...");
            }
            return commandes[0];
        }
        catch(Exception e){
            throw e;
        }
        finally{
            db.close();
        }
    }
}
