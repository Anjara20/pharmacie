/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author je sais pas
 */
public class DetailCommandeService 
{
    String iddetailcommande;
    String idcommande;
    int quantiteproduit;
    String nomproduit;
    double prixunitaire;
    
    public DetailCommandeService()
    {
        
    }

    public DetailCommandeService(String iddetailcommande, String idcommande, int quantiteproduit, String nomproduit, double prixunitaire) 
    {
        setIddetailcommande(iddetailcommande);
        setIdcommande(idcommande);
        setQuantiteproduit(quantiteproduit);
        setNomproduit(nomproduit);
        setPrixunitaire(prixunitaire);
    }

    public DetailCommandeService(String idcommande, int quantiteproduit, String nomproduit, int prixunitaire) 
    {
        setIddetailcommande("'det'||nextval('seq_detailcommande')");
        setIdcommande("com" + idcommande);
        setQuantiteproduit(quantiteproduit);
        setNomproduit(nomproduit);
        setPrixunitaire(0);
    }

    public String getIddetailcommande() 
    {
        return iddetailcommande;
    }

    public void setIddetailcommande(String iddetailcommande) 
    {
        this.iddetailcommande = iddetailcommande;
    }

    public String getIdcommande() 
    {
        return idcommande;
    }

    public void setIdcommande(String idcommande) 
    {
        this.idcommande = idcommande;
    }

    public int getQuantiteproduit() 
    {
        return quantiteproduit;
    }

    public void setQuantiteproduit(int quantiteproduit) 
    {
        this.quantiteproduit = quantiteproduit;
    }
    
    public void setQuantiteproduit(String quantiteproduit) 
    {
        int qte = Integer.parseInt(quantiteproduit);
        setQuantiteproduit(qte);
    }

    public String getNomproduit() 
    {
        return nomproduit;
    }

    public void setNomproduit(String nomproduit) 
    {
        this.nomproduit = nomproduit;
    }

    public double getPrixunitaire() 
    {
        return prixunitaire;
    }

    public void setPrixunitaire(double pu) 
    {
        this.prixunitaire = pu;
    }
    
    public void insert_detail_commande(Connection c) throws Exception
    {
        Statement req = null;
        try
        {
            System.out.println(this.getIdcommande());
            String sql = "insert into detailcommande values('det'||nextval('seq_detailcommande'),'" + this.getIdcommande() + "'," + this.getQuantiteproduit() + ",'" + this.getNomproduit().trim() + "'," + this.getPrixunitaire() + ")";
            System.out.println("sql = " + sql);
            req = c.createStatement();
            req.executeUpdate(sql);
            System.out.println("INSEREE");
        }
        catch(Exception e)
        {
            throw e;
        }
        finally
        {
            if(req != null)
            {
                req.close();
            }
        }
    }
}
