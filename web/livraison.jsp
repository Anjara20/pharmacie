<%@page import="model.CommandePrix"%>
<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Products - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/modernizer.js"></script>    
    <style>
        .prix{
            width: 16px;
        }
        .idDetail{
            width: 15px;
            color: red;
        }
        #taille{
            display: none;
        }
    </style>
</head>
<body>    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>Our Products</h2>
						<ul class="page-title-link">
							<li><a href="ModeleClient.jsp">Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div id="portfolio" class="section wb" style="margin-top:-100px">
        <div class="container">
            <div class="section-title text-center">
                <h3>Commander</h3>
            </div>
        </div>
        <div id="contenu">
            <% CommandePrix[] mesLivraisons = (CommandePrix[]) request.getAttribute("commandes");%>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nom du client</th>
                        <th>Contact</th>
                        <th>Adresse</th>
                        <th>Prix</th>
                        <th>D�tails</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    int count = mesLivraisons.length;
                    for(int i = 0 ; i<count ;i++){
                        %>
                            <tr>
                                <td><%out.println(mesLivraisons[i].getNomclient());%></td>
                                <td><%out.println(mesLivraisons[i].getContact());%></td>
                                <td><%out.println(mesLivraisons[i].getAdresse());%></td>
                                <td><%out.println(mesLivraisons[i].getPrix());%></td>
                                <td><a href="./details?commande=<%out.println(mesLivraisons[i].getIdCommande());%>">D�tails</a></td>
                            </tr>
                            <%
                        }
                    %>
                </tbody>
                
            </table>
        </div>
    </div>

    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="js/all.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/portfolio.js"></script>
    <script src="js/hoverdir.js"></script>    

</body>
</html>