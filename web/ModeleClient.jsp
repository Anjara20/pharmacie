<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Home - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="./images/apple-touch-icon.png">

    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="./css/responsive.css">
    <link rel="stylesheet" href="./css/custom.css">

    <script src="./js/modernizer.js"></script>


</head>
<body>

   
    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <header class="header header_style_01">
        <nav class="megamenu navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp"><img src="images/logos/logo.png" alt="image"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right" style="margin-top:5px">
                        <li><a class="active" href="#">Home</a></li>
                        <li><a href="CommandeView.jsp">Commander</a></li>
                        <li><a href="demande-attente">Mes demandes</a></li>
                        <li><a href="login.jsp">Se connecter</a></li>
                        <li><a href="./deconnect">Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
	
	<div class="slider-area">
		<div class="slider-wrapper owl-carousel">
			<div class="slider-item home-one-slider-otem slider-item-four slider-bg-one">
				<div class="container">
					<div class="row">
						<div class="slider-content-area">
							<div class="slide-text">
								<h1 class="homepage-three-title">Outstanding <span>Installation</span> Services</h1>
								<h2>It's about e-Fanafody, thanks to this website, <br>you can make purchase of medicaments online  </h2>
								<div class="slider-content-btn">
									<a class="button btn btn-light btn-radius btn-brd" href="">Read More</a>
									<a class="button btn btn-light btn-radius btn-brd" href="contact.jsp">Contact</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slider-item text-center home-one-slider-otem slider-item-four slider-bg-two">
				<div class="container">
					<div class="row">
						<div class="slider-content-area">
							<div class="slide-text">
								<h1 class="homepage-three-title">Outstanding <span>Installation</span> Services</h1>
								<h2>It's about e-Fanafody, thanks to this website, <br> you can make purchase of medicaments online  </h2>
								<div class="slider-content-btn">
									<a class="button btn btn-light btn-radius btn-brd" href="">Read More</a>
									<a class="button btn btn-light btn-radius btn-brd" href="contact.jsp">Contact</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slider-item home-one-slider-otem slider-item-four slider-bg-three">
				<div class="container">
					<div class="row">
						<div class="slider-content-area">
							<div class="slide-text">
								<h1 class="homepage-three-title">Outstanding <span>Installation</span> Services</h1>
								<h2>It's about e-Fanafody, thanks to this website, <br> you can make purchase of medicaments online </h2>
								<div class="slider-content-btn">
									<a class="button btn btn-light btn-radius btn-brd" href="">Read More</a>
									<a class="button btn btn-light btn-radius btn-brd" href="contact.jsp">Contact</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div id="about" class="section wb">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="message-box">
                        <h4>About Us</h4>
                        <h2>Welcome to e-Fanafody</h2>
                        <p class="lead">Nowadays, thanks to the evolution of pharmaceutical practice and medicine, medicines became indispensable to the daily newspapers of society.</p>

                        <p> Access to these medicaments is no less easy to find in the Malagasy capital. This is caused by insecurity, the occupations of everyone, the time constraints and the need to wait to obtain pharmaceutical products.As a result, the objectives of this project are oriented towards the social and health domains but also a self-satisfaction of the creative group.</p>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="post-media wow fadeIn">
                        <img src="uploads/about_01.jpg" alt="" class="img-responsive img-rounded">
                        <a href="https://www.youtube.com/watch?v=l2ltMXI_aow" data-rel="prettyPhoto[gal]" class="playbutton"><i class="flaticon-play-button"></i></a>
                    </div>
                </div>
            </div>

            <hr class="hr1"> 

            <div class="row">
				<div class="col-md-6">
                    <div class="post-media wow fadeIn">
                        <img src="uploads/about_02.jpg" alt="" class="img-responsive img-rounded">
                    </div>
                </div>
				
                <div class="col-md-6">
                    <div class="message-box">
                        <h4>Who We are</h4>
                        <h2>We Are e-Fanafody</h2>
                        <p class="lead">This system will take root in Malagasy capital. For us, therefore, as Malagasy citizens, this project constitutes the greatest form of help that we can offer to the Nation. It is the synthesis and union of our achievements as educated citizens and the evolution brought by technology and globalization.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
    
    
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <img src="images/logos/logo-2.png" alt="" />
                        </div>
                        <p> Thanks to the system obtained by this project, the exchanges between the pharmaceutical shops and the customers will be considerably facilitated. The ease of access will allow the community to free itself from a burden, some worry and a little time, to devote itself to other things, without neglecting in a field as important and vital as health. The company benefiting from this system can therefore lighten up compared to their daily lives.</p>
                    </div>
                </div>

				<div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Pages</h3>
                        </div>

                        <ul class="footer-links hov">
                            <li><a href="index.jsp">Home <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="about-us.jsp">About <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="portfolio.jsp">Products <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="login.jsp">Login <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="">Panier <span class="icon icon-arrow-right2"></span></a></li>
							<li><a href="contact.jsp">Contact <span class="icon icon-arrow-right2"></span></a></li>
                        </ul>
                    </div>
                </div>
				
               
            </div>
        </div>
    </footer>

    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-left">                   
                    <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="">e-Fanafody</a>
                </div>

                
            </div>
        </div>
    </div>

    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="./js/all.js"></script>
    <script src="./js/custom.js"></script>
    <script src="./js/portfolio.js"></script>
    <script src="./js/hoverdir.js"></script>    

</body>
</html>