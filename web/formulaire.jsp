<%@page import="model.DetailCommande"%>
<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Products - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/modernizer.js"></script>    
    <style>
        .prix{
            width: 16px;
        }
        .idDetail{
            width: 15px;
            color: red;
        }
        #taille{
            display: none;
        }
    </style>
</head>
<body>    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>Our Products</h2>
						<ul class="page-title-link">
							<li><a href="ModeleClient.jsp">Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div id="portfolio" class="section wb" style="margin-top:-100px">
        <div class="container">
            <div class="section-title text-center">
                <h3>Facturer</h3>
            </div>
        </div>
        <div id="contenu" style="margin-left:35%">
           <% DetailCommande[] dtcmd=(DetailCommande[])request.getAttribute("DetailCommandes");%>
                <p id="taille"><%= dtcmd.length %></p>
                <form action="./valider" method="get">
                    <% for(int i=0;i<dtcmd.length;i++){
                    %>
                        <label for="prix1"><%=dtcmd[i].getQuantiteProduit()%> <%=dtcmd[i].getNomProduit()%></label>            
                        <input type="hidden" class="prix" id="q<%= (i+1)%>" value="<%= dtcmd[i].getQuantiteProduit() %>">
                        <input type="hidden" name="idDetailCommande<%= (i+1)%>" class="idDetail" value="<%= dtcmd[i].getIdDetailCommande() %>">
                        <input type="text" name="prixUnitaire<%= (i+1)%>" id="c<%= (i+1)%>">
                        
                         <br/>
                    <%}%>
                    <input type="hidden" name="idCommande" value="<%= dtcmd[0].getIdCommande() %>">
                   <input type="text" name="total" id="total">
                        <input type="hidden" name="nbFanafody" value="<%= dtcmd.length %>">
                        <input type="hidden" name="insertFacture" value="1">
                    <!-- <p id="aideMdp"></p> -->
                    <input type="submit" value="valider">
                </form>
        </div>
    
    </div>
        <script type="text/javascript">
                    // Vérification de la longueur du mot de passe saisi
                    //var prix=document.getElementsByName("prix1");
                    let test=0.0;
            var taille=parseInt(document.getElementById("taille"));
            var total = document.getElementById("total");
            let del=false;
            document.addEventListener("keydown",keyDownFunction,false);
            //document.addEventListener("keyup",keyUpFunction,false);
            function keyDownFunction(e){
                if(e.keyCode == 46 || e.keyCode == 8){//up key
                    console.log("   ato");
                    del = true;
                }
            }
            /*function keyUpFunction(e){
                if(e.keyCode == 46 || e.keyCode == 8){//up key
                    del = false;
                }
            }*/
            inp();
            function inp(){
                for (let i = 0; i< taille; i++) {
                    document.getElementById("c"+(i+1)).addEventListener("change", function (e) {
                        var mdp = e.target.value; // Valeur saisie dans le champ mdp
                        regexCourriel=/\d+|\d+[,]{1}/;
                        regexCourriel3=/[a-z]+|[,]{2,}/;
                        regexCourriel4=/[,]+/;
                        if (!regexCourriel.test(e.target.value)) {
                            e.target.value="";
                        }
                        if (regexCourriel3.test(e.target.value) &&regexCourriel.test(e.target.value)) {
                            e.target.value="";
                        }
                        if(del){
                            test=0.0;
                            del=false;
                            inp2();  
                        }
                        else{
                            var haha=e.target.value;
                            if(regexCourriel4.test(haha)){
                                haha=e.target.value.replace(regexCourriel4,'.')
                            }
                            if(parseFloat(haha)==NaN) test +=0;
                            else test +=parseFloat( haha*parseInt(document.getElementById("q"+(i+1)).value) );
                            total.value =test;  
                        }
                                    
                    });
                }
                function inp2(){
                    for (let i = 0; i< taille; i++) {
                        var haha=document.getElementById("c"+(i+1)).value;
                            if(regexCourriel4.test(haha)){
                                haha=document.getElementById("c"+(i+1)).value.replace(regexCourriel4,'.')
                            }
                            if(parseFloat(haha)==NaN) test +=0;
                            else test +=parseFloat(haha*parseInt(document.getElementById("q"+(i+1)).value));
                            total.value =test;
                    }
                }
            }
            
            /*
           
            }*/
        </script>
    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-left">                   
                    <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="">e-Fanafody</a>
                </div>

                
            </div>
        </div>
    </div>

    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="js/all.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/portfolio.js"></script>
    <script src="js/hoverdir.js"></script>    

</body>
</html>