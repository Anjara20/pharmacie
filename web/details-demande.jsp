<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Products - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/modernizer.js"></script>    
    <title>Leaflet map</title>
    <link rel="stylesheet" href="./js/leaflet/leaflet.css"/>
    <link rel="stylesheet" href="./js/leaflet-routing-machine/dist/leaflet-routing-machine.css" />
    <script src="./js/leaflet/leaflet.js"></script>
    <script src="./js/leaflet-routing-machine/dist/leaflet-routing-machine.js"></script>
    <script src="https://unpkg.com/esri-leaflet@2.2.4/dist/esri-leaflet.js"
    integrity="sha512-tyPum7h2h36X52O2gz+Pe8z/3l+Y9S1yEUscbVs5r5aEY5dFmP1WWRY/WLLElnFHa+k1JBQZSCDGwEAnm2IxAQ=="
    crossorigin=""></script>
  <!-- Load Esri Leaflet Geocoder from CDN -->
  <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.14/dist/esri-leaflet-geocoder.css"
    integrity="sha512-v5YmWLm8KqAAmg5808pETiccEohtt8rPVMGQ1jA6jqkWVydV5Cuz3nJ9fQ7ittSxvuqsvI9RSGfVoKPaAJZ/AQ=="
    crossorigin="">
  <script src="https://unpkg.com/esri-leaflet-geocoder@2.2.14/dist/esri-leaflet-geocoder.js"
    integrity="sha512-uK5jVwR81KVTGe8KpJa1QIN4n60TsSV8+DPbL5wWlYQvb0/nYNgSOg9dZG6ViQhwx/gaMszuWllTemL+K+IXjg=="
    crossorigin=""></script>
    <style>
    body { margin:0; padding:0; background-color: black}
    #map {
    width: 70%;
    height: 70vh;}
  </style>
</head>
<body>    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>Details</h2>
						<ul class="page-title-link">
							<li><a href="ModeleClient.jsp">Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div id="portfolio" class="section wb" style="margin-top:-100px;background-color: black">
        <div class="container">
            <div class="section-title text-center">
                <h3 style="color:whitesmoke">D�tails Livraison</h3>
            </div>
        </div>
        <div id="contenu" style="margin-left:20%">
           <div id="map"></div>
            <form action="" method="POST">
                <input type="hidden" id="firstPoint" value="<% out.println(request.getParameter("client"));%>">
                <input type="hidden" id="lastPoint" value="<% out.println(request.getParameter("pharmacie"));%>">
            </form>
            <script src="./js/app-demande.js"></script>
        </div>
    </div>
<div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-left">                   
                    <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="">e-Fanafody</a>
                </div>

                
            </div>
        </div>
    </div>
    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="js/all.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/portfolio.js"></script>
    <script src="js/hoverdir.js"></script>    

</body>
</html>