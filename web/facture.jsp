<!DOCTYPE html>
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
    <title>Products - e-Fanafody</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">

    <script src="js/modernizer.js"></script>    
    <style>
        .prix{
            width: 16px;
        }
        .idDetail{
            width: 15px;
            color: red;
        }
        #taille{
            display: none;
        }
    </style>
</head>
<body>    
	<div class="top-bar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="left-top">
						<div class="email-box">
							<a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i> e.fanafody@gmail.com</a>
						</div>
						<div class="phone-box">
							<a href="tel:261340456789"><i class="fa fa-phone" aria-hidden="true"></i> +261 34 04 567 89</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="right-top">
						<div class="social-box">
							<ul>
								<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
								<li><a href=""><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
							<ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
   	<div class="banner-area banner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="banner">
						<h2>Facturer</h2>
						<ul class="page-title-link">
							<li><a href="ModeleClient.jsp">Home</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div id="portfolio" class="section wb" style="margin-top:-100px">
        <div class="container">
            <div class="section-title text-center">
                <h3>Facture</h3>
            </div>
        </div>
        <div id="contenu">
            <% String facture = (String) request.getAttribute("pdf");%>
            <object data="<%out.println(facture);%>" type="application/pdf" width="600" height="500" style="float:left;margin-left: 5%">
              alt : <a href="<%out.println(facture);%>">facture.pdf</a>
            </object>
            <form action="./confirmer" method="POST" style="float:right;margin-right:35%;margin-top: 5%"> 
                <input type="hidden" name="idCommande" value="<%out.println(request.getParameter("idCommande"));%>">
                <button type="submit" name="answer" value="accepter" class="btn btn-success">accepter</button>
                <button type="submit" name="answer" value="refuser" class="btn btn-secondary">refuser</button>
            </form>
                </div>
            
            </div>
            <div class="copyrights">
                <div class="container">
                    <div class="footer-distributed">
                        <div class="footer-left">                   
                            <p class="footer-company-name">All Rights Reserved. &copy; 2018 <a href="">e-Fanafody</a>
                        </div>

                        
                    </div>
            </div>
    </div>

    <a href="" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <script src="js/all.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/portfolio.js"></script>
    <script src="js/hoverdir.js"></script>    

</body>
</html>