/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var zoom = 13;
var tana = {latitude:-18.9100122, longitude:47.5255809};
var mymap = L.map('map').setView([tana.latitude,tana.longitude], 13);
// Coordonées où notre map est centré
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
maxZoom: 19,
attribution: ' <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(mymap);
var firstPoint = document.getElementById("firstPoint").value;
var firstCoordinates = {lng:firstPoint.split('(')[1].split(' ')[0],lat:firstPoint.split(' ')[1].split(')')[0]};
var lastPoint = document.getElementById("lastPoint").value;
var lastCoordinates = {lng:lastPoint.split('(')[1].split(' ')[0],lat:lastPoint.split(' ')[1].split(')')[0]};
mymap.panTo(new L.LatLng(firstCoordinates.lat,firstCoordinates.lng));

var routesControl =  L.Routing.control({
    waypoints: [
      L.latLng(firstCoordinates.lat,firstCoordinates.lng),
      L.latLng(lastCoordinates.lat,lastCoordinates.lng)
    ]
  }).addTo(mymap);