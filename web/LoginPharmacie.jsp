<%-- 
    Document   : LoginPharmacie
    Created on : 7 août 2019, 22:39:56
    Author     : je sais pas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Object erreur = request.getAttribute("erreur");
            if(erreur != null)
            {
                String error = (String)erreur;
        %>
                <div class="alert alert-danger"> 
                    <i class="icon icon-times-circle icon-lg"></i>
                    <strong>Erreur !</strong> <% out.println(error); %>.
                </div>
        <%
            }
        %>
        <form role="form" name="form" action="login-pharmacie" method="post">
            <h4>Votre nom</h4><input name="nompharmacie" type="text" />
            <button type="submit" class="btn btn-success"> Connexion </button>
        </form>
    </body>
</html>
